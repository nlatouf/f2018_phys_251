# author: Natasha Latouf

# Homework 8

# Problem 1

import numpy as np
import matplotlib.pyplot as plt

# Part A

# Input the Initial Conditions
y0 = 0
# Time
ti = 0
tf = 1.0
# Set up an array for all the different delta t's
dtarray = np.asarray([.5, .1, .05, .01])
# Form a for loop to call each array element, and calculate nt
for it in range(len(dtarray)):
    dt = dtarray[it]
    nt = int((tf - ti) / dt) + 1
    # Solve for times
    t = np.linspace(ti, tf, num=nt)
    # Set up an empty array to catch the values
    y = np.zeros(nt)

    y[0] = y0
    # Make a nested for loop for solving the given equation
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (t[k] * np.exp(3 * t[k]) - 2 * y[k])
    # Plot each line together, with different labels
    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))
# Label the axises, title, and add a grid, then clear the plot
plt.xlabel('Time (s)')
plt.ylabel('y')
plt.title('Part A')
plt.legend()
plt.grid(True)
# plt.show()
plt.clf()

# Part B
dtarray = np.asarray([.5, .1, .05, .01])
# Repeat for every equation
y0 = 1

ti = 2
tf = 3

for it in range(len(dtarray)):
    dt = dtarray[it]
    nt = int((tf - ti) / dt) + 1

    t = np.linspace(ti, tf, num=nt)

    y = np.zeros(nt)

    y[0] = y0
    # Be sure to change the equation to the one for each problem
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (1 + (t[k] - y[k])**2)

    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))

plt.xlabel('Time (s)')
plt.ylabel('y')
plt.title('Part B')
plt.legend()
plt.grid(True)
plt.show()
plt.clf()

# Part C
# Repeat
ti = 1
tf = 2
y0 = 1
# Make a new array, since new values are called for!
dtarray1 = np.asarray([.25, .1, .05, .01])

for it in range(len(dtarray1)):
    dt = dtarray1[it]
    nt = int((tf - ti) / dt) + 1

    t = np.linspace(ti, tf, num=nt)

    y = np.zeros(nt)

    y[0] = y0
    # Change equation
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (1 + (y[k] / t[k]))

    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))

plt.xlabel('Time (s)')
plt.ylabel('y')
plt.title('Part C')
plt.legend()
plt.grid(True)
plt.show()
plt.clf()

# Part D
# Repeat
ti = 0
tf = 2*np.pi
y0 = 0

for it in range(len(dtarray1)):
    dt = dtarray1[it]
    nt = int((tf - ti) / dt) + 1

    t = np.linspace(ti, tf, num=nt)

    y = np.zeros(nt)

    y[0] = y0
    # Change equation
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (np.cos(2*t[k]) + np.sin(3*t[k]))

    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))


plt.xlabel('Time (s)')
plt.ylabel('y')
plt.title('Part D')
plt.legend()
plt.grid(True)
plt.show()
plt.clf()
