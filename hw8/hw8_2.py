# author: Natasha Latouf

# Homework 8

# Problem 2

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

# Define all of the functions, to use later to find the true values of the ODEs
def dydt(y, t):
    F = np.exp(t - y)
    return F


def dydt2(y, t):
    F = t**2 * (np.sin(2*t) - 2 * t * y)
    return F


def dydt3(y, t):
    F = -y + t * y ** (1/2)
    return F


def dydt4(y, t):
    F = ((t * y) + y)/((t * y) + t)
    return F

# Part A


# Input Initial Conditions
y0 = 1
# Time
ti = 0
tf = 1.0
# Establish the delta T array
dtarray = np.asarray([.5, .1, .05, .01])
# Establish a for loop to call each dt value
for it in range(len(dtarray)):
    dt = dtarray[it]
    nt = int((tf - ti) / dt) + 1

    t = np.linspace(ti, tf, num=nt)

    y = np.zeros(nt)

    y[0] = y0
    # Input the equation given
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (np.exp(t[k] - y[k]))
    # Plot each line
    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))
# Set up the time again, and a new y value, that will be the true ODE Value
ts = np.linspace(ti, tf, num=4)
ys = odeint(dydt, y0, ts)
# Plot the true values with the approximations
plt.plot(ts, ys, 'x', label='odeint')
plt.xlabel('Time (s)')
plt.ylabel('y')
plt.title('Part A')
plt.legend()
plt.grid(True)
plt.show()
plt.clf()

# Part B
# Repeat above steps for each equation
ti = 1
tf = 2
y0 = 2

for it in range(len(dtarray)):
    dt = dtarray[it]
    nt = int((tf - ti) / dt) + 1

    t = np.linspace(ti, tf, num=nt)

    y = np.zeros(nt)

    y[0] = y0
    # Change equation
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (t[k]**2 * (np.sin(2*t[k]) - 2 * t[k] * y[k]))

    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))

ts = np.linspace(ti, tf, num=4)
ys = odeint(dydt2, y0, ts)

plt.plot(ts, ys, 'x', label='odeint')
plt.xlabel('Time (s)')
plt.ylabel('y')
plt.legend()
plt.title('Part B')
plt.grid(True)
plt.show()
plt.clf()

# Part C
# Repeat above steps
# Establish a new array for new dt values given!
dtarray1 = np.asarray([.25, .1, .05, .01])

ti = 2
tf = 3
y0 = 2

for it in range(len(dtarray1)):
    dt = dtarray1[it]
    nt = int((tf - ti) / dt) + 1

    t = np.linspace(ti, tf, num=nt)

    y = np.zeros(nt)

    y[0] = y0
    # Change equation
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (-y[k] + t[k] * y[k] ** (1/2))

    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))

ts = np.linspace(ti, tf, num=4)
ys = odeint(dydt3, y0, ts)

plt.plot(ts, ys, 'x', label='odeint')
plt.xlabel('Time (s)')
plt.ylabel('y')
plt.title('Part C')
plt.legend()
plt.grid(True)
plt.show()
plt.clf()

# Part D
# Repeat above steps
ti = 2
tf = 4
y0 = 4

for it in range(len(dtarray1)):
    dt = dtarray1[it]
    nt = int((tf - ti) / dt) + 1

    t = np.linspace(ti, tf, num=nt)

    y = np.zeros(nt)

    y[0] = y0
    # Change Equation
    for k in range(nt - 1):
        y[k + 1] = y[k] + dt * (((t[k] * y[k]) + y[k])/((t[k] * y[k]) + t[k]))

    plt.plot(t, y, '.-', label='\u0394t = ' + str(dt))

ts = np.linspace(ti, tf, num=4)
ys = odeint(dydt4, y0, ts)

plt.plot(ts, ys, 'x', label='odeint')
plt.xlabel('Time (s)')
plt.ylabel('y')
plt.title('Part D')
plt.legend()
plt.grid(True)
plt.show()
plt.clf()
