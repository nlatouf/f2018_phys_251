# author: Natasha Latouf

# Homework 5

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# Problem 2
# Import in the necessary file
dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Documents'
dname2 = r'PHYS251'
fname = 'fc_thist_00520.txt'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
thist = np.genfromtxt(dfname, skip_header=2)
# Retrieve all the variables from each column, as described by the question
# Time
t = thist[:, 0]
# Pressure
p = thist[:, 1]
# Velocity Vector Components
x = thist[:, 2]
y = thist[:, 3]
z = thist[:, 4]
# Norm of velocity vector
n = thist[:, 5]
# Temperature
temp = thist[:, 6]
# Concentration of H2S in ppm
s1 = thist[:, 7]
s2 = thist[:, 8]
s3 = thist[:, 9]
s4 = thist[:, 10]
s5 = thist[:, 11]
# Plot time vs the scalar quantity s1
plt.plot(t, s1, '.')
plt.xlabel('Concentration of H2S (ppm)')
plt.ylabel('Time (s)')
plt.title('S1 vs Time')
plt.grid()
# plt.show()
# Make a new array only containing points 1-400, and set up an empty matrix for s1
tnew = np.arange(1, 401)
s1new = np.zeros(400)
# print(tnew)

# Set up a for loop where the slope will be found at each set of points, to account for the change in slope
# Then input the given equation again, this time with the upper and lower indices
for i in range(tnew.size):
    lower = np.where(t < tnew[i])[0][-1]
    upper = np.where(t > tnew[i])[0][0]
    s1new[i] = s1[lower] + (tnew[i] - t[lower]) * (s1[upper] - s1[lower]) / (t[upper] - t[lower])
# Plot the interpolated data
plt.plot(tnew, s1new, marker='.')
plt.grid()
plt.ylabel('Time (s)')
plt.xlabel('Concentration of H2S (ppm)')
plt.title('Interpolated Data')
# plt.show()
plt.clf()
# Make a new array with points 1 - 400, growing by 10
tten = np.arange(1, 402, 10)
# Repeat the same for loop as above
for i in range(tten.size):
    lower = np.where(t < tnew[i])[0][-1]
    upper = np.where(t > tnew[i])[0][0]
    s1new[i] = s1[lower] + (tnew[i] - t[lower]) * (s1[upper] - s1[lower]) / (t[upper] - t[lower])
# Plot the new interpolated data
plt.plot(tnew, s1new, marker='.')
plt.grid()
plt.ylabel('Time (s)')
plt.xlabel('Concentration of H2S (ppm)')
plt.title('Interpolation with Steps of 10')
# plt.show()
plt.clf()
