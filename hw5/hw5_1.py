# author: Natasha Latouf

# Homework 5

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

# Problem 1
# Import in the needed file
dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Documents'
dname2 = r'PHYS251'
fname = 'velocity_run18.txt'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
velo = np.genfromtxt('/Users/natashalatouf/Documents/PHYS251/velocity_run18.txt', skip_header=1)
# Extract the time, velocity, and error from the data
t = velo[:, 0]
v = velo[:, 1]
sigv = velo[:, 2]
# Plot the original data, with a grid, labels, error bars, and title
plt.plot(t, v, '.', linewidth=0)
plt.errorbar(t, v, yerr=sigv, linewidth=0, marker='.')
plt.ylabel('Velocity (m/s)')
plt.xlabel('Time (s)')
plt.title('Original Run')
plt.grid()
# plt.show()
plt.clf()

# Make a new array in which to fill the new values, minus 1 to account for the last one
t_new = np.zeros(t.size-1)
v_new = np.zeros(t.size-1)
# To find t[i + 1/2], find the average of t[i] and t[i+1]
for i in range(t_new.size):
    t_new[i] = (t[i]+t[i+1])/2
# Plug in all the variables into the equation we are given
for i in range(t_new.size):
    v_new[i] = v[0] + (t_new[i]-t[0]) * (v[1]-v[0]) / (t[1]-t[0])

# Plot the new interpolated data
plt.plot(t_new, v_new)
plt.title('Interpolated Data')
plt.xlabel('Velocity (m/s)')
plt.ylabel('Time (s)')
# plt.show()
plt.clf()

# Define the optimal function, which is a line in this case


def mymodel(t, m, b):
    v = m * t + b
    return v

# Set up the popt and pcov matrices, and then retrieve the new m and b terms


popt, pcov = curve_fit(mymodel, t, v)
m1 = popt[0]
b1 = popt[1]
# Redefine a function with the optimal m and b terms
yhat = mymodel(t, m1, b1)
# Plot the curve_fit data and the original data together
plt.scatter(t, v, label='Original')
plt.plot(t, yhat, label='Fit Curve')
plt.xlabel('Velocity (m/s)')
plt.ylabel('Time (s)')
plt.title('Data vs Fit Curve')
plt.grid()
# plt.show()
plt.clf()

