# author: Natasha Latouf

# Homework 5
# Import the necessary libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

# Problem 3

# Import the necessary file
dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Documents'
dname2 = r'PHYS251'
fname = 'data_2D_grid_T.txt'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
grid = np.genfromtxt(dfname, skip_header=13)
# Bring in the variables from the file
x = grid[:, 0]
y = grid[:, 1]
z = grid[:, 2]
T = grid[:, 3]
# Find the bounds of the data
nx = (max(x) - min(x))/.5 + 1
ny = (max(y) - min(y))/.5 + 1
# Make them integers
nx = int(nx)
ny = int(ny)
# Reshape x, y, z to be the same shape as the integers nx and ny
xx = x.reshape((nx, ny))
yy = y.reshape((nx, ny))
zz = z.reshape((nx, ny))
# Plot the figure in 3D
fig1 = plt.figure(1)
ax1 = fig1.gca(projection='3d')
surf = ax1.plot_surface(xx, yy, zz, cmap=cm.inferno, linewidth=0)
plt.title('3D Plot')
# plt.show()
# Set up matrices for the new variables to be calculated
znew = np.zeros((nx - 1, ny - 1))
xnew = np.zeros((nx - 1, ny - 1))
ynew = np.zeros((nx - 1, ny - 1))
# Set up a for loop that will create the new x, y, and z values, based on the given formula
# Note that changes in x are shown in j, and changes in y are shown in i
for i in range(nx - 1):
    for j in range(ny - 1):
        xnew[i, j] = (xx[i, j + 1] + xx[i, j]) / 2
        ynew[i, j] = (yy[i + 1, j] + yy[i, j]) / 2
        a = zz[i, j] * (xx[i, j + 1] - xnew[i, j]) * (yy[i + 1, j] - ynew[i, j])
        b = zz[i + 1, j] * (xx[i, j + 1] - xnew[i, j]) * (ynew[i, j] - yy[i, j])
        c = zz[i, j + 1] * (xnew[i, j] - xx[i, j]) * (yy[i + 1, j] - ynew[i, j])
        d = zz[i + 1, j + 1] * (xnew[i, j] - xx[i, j]) * (ynew[i, j] - yy[i, j])
        znew[i, j] = (a + b + c + d) / ((xx[i, j + 1] - xx[i, j]) * (yy[i + 1, j] - yy[i, j]))
# Plot the interpolated data
fig2 = plt.figure(2)
ax2 = fig2.gca(projection='3d')
surf = ax2.plot_surface(xnew, ynew, znew, cmap=cm.inferno, linewidth=0)
plt.title('Interpolated 3D Plot')
# plt.show()

