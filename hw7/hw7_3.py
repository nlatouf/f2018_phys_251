# author: Natasha Latouf

# Homework 7

import numpy as np
import matplotlib.pyplot as plt

# Problem 3


# Define the analytical definition of the derivative
def dfdx_a(x):
    return -2*x*np.exp(-x**2)


# Define the analytical definition of the second derivative
def dfdx_a2(x):
    return ((4*x**2) - 2) * np.exp(-x**2)


# Define the definition for the forward difference
def dfdx_f(f, x, dx):
    return (f(x + dx) - f(x))/dx


# Define the definition for the backward difference
def dfdx_b(f, x, dx):
    return (f(x) - f(x - dx))/dx


# Define the defintion for the central difference
def dfdx_c(f, x, dx):
    return (f(x + dx) - f(x - dx))/2*dx


# Define the definition for the second order of the central difference
def dfdx_c2(f, x, dx):
    return (f(x + dx) - 2 * f(x) + f(x - dx))/dx**2


# Define the given equation
def f(x):
    return np.exp(-x**2)


# Define the bounds, and the number of points
x = np.linspace(-5, 5, 200)
x1 = np.array([0, 1])
# Set dx to be as small as possible, for accuracy
dx = 1e-4
# Test the values with a for loop to ensure accuracy
for i in range(len(x1)):
    fx = f(i)
    dfdxa = dfdx_a(i)
    dfdxa2 = dfdx_a2(i)
    dfdxf = dfdx_f(f, i, dx)
    dfdxb = dfdx_b(f, i, dx)
    dfdxc = dfdx_c(f, i, dx)
    dfdxc2 = dfdx_c2(f, i, dx)
    print(fx)
    print(dfdxa)
    print(dfdxa2)
    print(dfdxf)
    print(dfdxb)
    print(dfdxc)
    print(dfdxc2)

# Call the functions, and rename them
fx = f(x)
dfdxa = dfdx_a(x)
dfdxa2 = dfdx_a2(x)
dfdxf = dfdx_f(f, x, dx)
dfdxb = dfdx_b(f, x, dx)
dfdxc = dfdx_c(f, x, dx)
dfdxc2 = dfdx_c2(f, x, dx)

# Plot the original function, and each derivative with axis labels, a title, and a legend
plt.plot(x, fx, label='Original')
plt.plot(x, dfdxa, '.', label='Analytical')
plt.plot(x, dfdxa2, '.', label=' Second Analytical')
plt.plot(x, dfdxf, 'x', label='Forward')
plt.plot(x, dfdxb, label='Backward')
plt.plot(x, dfdxc, label='Central')
plt.plot(x, dfdxc2, label='Second Central')
plt.legend()
plt.grid()
plt.xlabel('x')
plt.ylabel('df/dx')
plt.title('e^-x**2')
plt.show()