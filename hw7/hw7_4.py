# author: Natasha Latouf

# Homework 7

# Problem 4

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def dfdx_a(x):
    return -2*x*np.exp(-x**2)


# Define the definition for the forward difference
def dfdx_f(f, x, dx):
    return (f(x + dx) - f(x))/dx


# Define the definition for the backward difference
def dfdx_b(f, x, dx):
    return (f(x) - f(x - dx))/dx


# Define the defintion for the central difference
def dfdx_c(f, x, dx):
    return (f(x + dx) - f(x - dx))/2*dx


# Define the third approximation derivative
def dfdx_3(f, x, dx):
    return (1/2*dx**3)*(f(x + 2*dx) - 2*f(x + dx) + 2*f(x - dx) - f(x - 2*dx))


# Define the given equation
def f(x):
    return np.exp(-x**2)


# Set the variable given in the question
x = 1
# Make an array of dx, arbitrarily
dx = np.array([1e-4, 2e-4, 5e-4, 1e-3, 2e-3, 5e-3, 1e-2, 2e-2, 5e-2, 1e-1, 2e-1, 5e-1])


# Define the function that calculates lower case delta, the error between the approximations, for forward
def dfunc(f, x, dx):
    n = len(dx)
    delta = np.empty(n)
    for i in range(n):
        delta[i] = abs(dfdx_a(x) - dfdx_f(f, x, dx[i]))
    return delta


# Repeat function for backward
def dfunc1(f, x, dx):
    n = len(dx)
    delta1 = np.empty(n)
    for i in range(n):
        delta1[i] = abs(dfdx_a(x) - dfdx_b(f, x, dx[i]))
    return delta1


# Repeat for central
def dfunc2(f, x, dx):
    n = len(dx)
    delta2 = np.empty(n)
    for i in range(n):
        delta2[i] = abs(dfdx_a(x) - dfdx_c(f, x, dx[i]))
    return delta2


# Repeat for the 3rd approximation
def dfunc3(f, x, dx):
    n = len(dx)
    delta3 = np.empty(n)
    for i in range(n):
        delta3[i] = abs(dfdx_a(x) - dfdx_3(f, x, dx[i]))
    return delta3


# Call each function, and rename
delta = dfunc(f, x, dx)
delta1 = dfunc1(f, x, dx)
delta2 = dfunc2(f, x, dx)
delta3 = dfunc3(f, x, dx)


# Define the model for a line
def fmodel(x, m, b):
    return m * x + b


# Define a variable to contain the calculations for dx and each delta
x = np.log10(dx)
y = np.log10(delta)
y1 = np.log10(delta1)
y2 = np.log10(delta2)
y3 = np.log10(delta3)
# Curve fit the data to the proper model
popt, pcov = curve_fit(fmodel, x, y)
popt1, pcov1 = curve_fit(fmodel, x, y1)
popt2, pcov2 = curve_fit(fmodel, x, y2)
popt3, pcov3 = curve_fit(fmodel, x, y3)

# Call the new variables
m = popt[0]
b = popt[1]

m1 = popt1[0]
b1 = popt1[1]

m2 = popt2[0]
b2 = popt2[1]

m3 = popt3[0]
b3 = popt3[1]

# Rename the equations, and raise it to the tenth, to account for the log in the equation
yhat = fmodel(x, m, b)
yhat = 10 ** yhat

yhat1 = fmodel(x, m1, b1)
yhat1 = 10 ** yhat1

yhat2 = fmodel(x, m2, b2)
yhat2 = 10 ** yhat2

yhat3 = fmodel(x, m3, b3)
yhat3 = 10 ** yhat3
# Plot all the errors with all the dx's, along with all of the fitted curves
plt.loglog(dx, delta, '.', label='FD Approximation')
plt.loglog(dx, delta1, '*', label='BD Approximation')
plt.loglog(dx, delta2, '^', label='CD Approximation')
plt.loglog(dx, delta3, 'd', label='3rd Approximation')
plt.loglog(dx, yhat, '-.')
plt.loglog(dx, yhat1, '-.')
plt.loglog(dx, yhat2, '-.')
plt.loglog(dx, yhat3, '-.')
plt.grid(True)
plt.title('Plot Remake')
plt.legend()
plt.xlabel('\u0394x')
plt.ylabel('\u03B4')
plt.show()
