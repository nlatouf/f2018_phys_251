# author: Natasha Latouf

# Homework 7

import numpy as np
import matplotlib.pyplot as plt

# Problem 1


# Define the analytical definition of the derivative
def dfdx_a(x):
    return -np.sin(x) + np.cos(x)


# Define the definition for the forward difference
def dfdx_f(f, x, dx):
    return (f(x + dx) - f(x))/dx


# Define the definition for the backward difference
def dfdx_b(f, x, dx):
    return (f(x) - f(x - dx))/dx


# Define the defintion for the central difference
def dfdx_c(f, x, dx):
    return (f(x + dx) - f(x - dx))/2*dx


# Define the given equation
def f(x):
    return np.cos(x) + np.sin(x)


# Define the bounds, and the number of points
x = np.linspace(0, 2*np.pi, 100)
x1 = np.array([0, 1])
# Set dx to be as small as possible, for accuracy
dx = 1e-4
# Test the values with a for loop to ensure accuracy
for i in range(len(x1)):
    fx = f(i)
    dfdxa = dfdx_a(i)
    dfdxf = dfdx_f(f, i, dx)
    dfdxb = dfdx_b(f, i, dx)
    dfdxc = dfdx_c(f, i, dx)
    print(fx)
    print(dfdxa)
    print(dfdxf)
    print(dfdxb)
    print(dfdxc)

# Call the functions, and rename them
fx = f(x)
dfdxa = dfdx_a(x)
dfdxf = dfdx_f(f, x, dx)
dfdxb = dfdx_b(f, x, dx)
dfdxc = dfdx_c(f, x, dx)

# Plot the original function, and each derivative with axis labels, a title, and a legend
plt.plot(x, fx, label='Original')
plt.plot(x, dfdxa, '.', label='Analytical')
plt.plot(x, dfdxf, 'x', label='Forward')
plt.plot(x, dfdxb, label='Backward')
plt.plot(x, dfdxc, label='Central')
plt.legend()
plt.grid()
plt.xlabel('x')
plt.ylabel('df/dx')
plt.title('cos(x) + sin(x)')
plt.show()
