# author: Natasha Latouf

# Problem 4
import numpy as np
from scipy.integrate import quad


# Define a function detailing the general formula of the trapezoidal rule
def my_simpson(f, a, b, n):
    # Figure out the change in x
    dx = (b - a)/n
    # Account for the change in Simpson's method
    h = dx/6
    # The area begins at 0
    area = 0
    # Set up the left end point as the first end point
    xl = a
    # Set up a for loop for calculating the area
    for i in range(n):
        # Input the equation for the midpoint
        xmid = xl + dx/2
        # Input the equation for the right end point
        xr = xl + dx
        # Calculate the area due to Simpson's method
        area = area + f(xl) + 4*f(xmid) + f(xr)
        # Account for the moving rectangle by shifting the left end point to the right end point
        xl = xr
    # Multiply the area by the change in x
    area = area * h
    return area


# Define a function detailing the general formula of the trapezoidal rule
def my_trapezoidal(f, n, a, b):
    # Find the width of the rectangle part of the trapezoid
    x, w = np.linspace(a, b, num=n+1, retstep=True)
    # Find the change in x between points
    h = (b - a)/n
    # Find the initial condition
    s = f(x[0])
    # Set up a for loop to loop through the different values of n
    for i in range(1, n):
        s += 2 * f(x[i])
    # Return the final answer for the trapezoidal area
    return (s + f(x[-1])) * h/2

# Input the bounds and the number of trapezoids


a = 0
b = 4
n = 2


# Define the original function
def f(x):
    return ((x**3)/81) + 1


# Print the value of Simpson's, trapezoidal, the integral, and the error
As = my_simpson(f, a, b, n)
print(As)

At = my_trapezoidal(f, n, a, b)
print(At)

[Af, error] = quad(f, a, b)
print(Af)

er = abs((At - As)/As)
print(er)


# Define the precision we want to reach
p = 10 ** -2


# Define a new function, in order to find the new number of trapezoids
def trap_num(f, a, b):
    # Set n as zero to begin with, and delta to be 1
    n = 0
    d = 1
    # Set up a while loop that will loop as long as delta is larger than the needed precision
    while d > p:
        # Set n to go up by 1 with each iteration
        n += 1
        # Input the calculations straight from the defined trapezoidal function
        x, w = np.linspace(a, b, num=n+1, retstep=True)
        h = (b - a)/n
        s = f(x[0])
        for i in range(1, n):
            s += 2 * f(x[i])
        area = (s + f(x[-1])) * h/2
        # Calculate the error from each new area
        d = abs((area - As)/As)
    return n


# Print the n value that is needed
print(trap_num(f, a, b))
