# author: Natasha Latouf

# Homework 6

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad

# Middle rectangle

# Establish the bounds
a = 0
b = 1

# Set up empty arrays to fill in the values
y = np.zeros(100)
A = np.zeros(100)
# Set up the array for N
N = np.linspace(1, 100, num=100, dtype=int)
An = np.zeros(100)
# Set up a for loop to calculate the midpoint, and then calculate the area of each rectangle
# Also set up a sum to calculate the total area under the curve
# Set up a for loop using the above made array for N
for n in N:
    for i in range(n):
        # Find the x value and width of each rectangle
        x, w = np.linspace(a, b, num=n + 1, retstep=True)
        y[i] = (i+1 - (1/2)) * ((b - a)/n)
        A[i] = w*(y[i]**2)
    An[n-1] = sum(A)

# Print the value of the midpoint area approximation
print(An)

# Set up the actual equation
fc_pol = lambda x: x**2

# Use scipy to integrate the function over the bounds
[At, error] = quad(fc_pol, a, b)
# Print the value of the integrated area
print(At)

# Calculate the error between the two area approximation
er = np.zeros(100)
er = np.abs((An - At)/At)

# Print the error value
# print(er)
# Plot the error vs the number of rectangles
plt.plot(N, er, '.', label='Error')
# Add a legend, plot, axis labels, and a title, then save the figure
plt.legend()
plt.grid()
plt.xlabel('Error')
plt.ylabel('Number of Rectangles')
plt.title('Error vs # of Rectangles')
plt.show()
plt.savefig('hw6_1.png')
plt.clf()
