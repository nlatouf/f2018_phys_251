# author: Natasha Latouf

# Problem 3

import numpy as np
from scipy.integrate import quad
import math


# Define a function detailing the general formula of the trapezoidal rule
def my_simpson(f, a, b, n):
    # Figure out the change in x
    dx = (b - a)/n
    # Account for the change in Simpson's method
    h = dx/6
    # The area begins at 0
    area = 0
    # Set up the left end point as the first end point
    xl = a
    # Set up a for loop for calculating the area
    for i in range(n):
        # Input the equation for the midpoint
        xmid = xl + dx/2
        # Input the equation for the right end point
        xr = xl + dx
        # Calculate the area due to Simpson's method
        area = area + f(xl) + 4*f(xmid) + f(xr)
        # Account for the moving rectangle by shifting the left end point to the right end point
        xl = xr
    # Multiply the area by the change in x
    area = area * h
    return area


# Define all of the functions in the problem
def f(x):
    return x**4


def g(x):
    return 2/(x - 4)


def c(x):
    return (x**2)*(math.log(x))


def t(x):
    return np.exp(2*x)*np.sin(2*x)


# Repeat the steps from the trapezoidal code, by setting up an array and inputting the array
a = 0
b = 4
n = np.array([1, 10, 100, 1000])
# Set up a for loop to calculate the area using the Simpson's method for each different function
for i in range(len(n)):
    print(my_simpson(f, a, b, n[i]))
# Calculate the analytical integral value, and print all values
[Af, error] = quad(f, a, b)
print(Af)
# Repeat for every function
a = 0
b = 3

for i in range(len(n)):
    print(my_simpson(g, a, b, n[i]))

[Ag, error] = quad(g, a, b)
print(Ag)


a = 1
b = 4

for i in range(len(n)):
    print(my_simpson(c, a, b, n[i]))

[Ac, error] = quad(c, a, b)
print(Ac)


a = 0
b = 2*np.pi

for i in range(len(n)):
    print(my_simpson(t, a, b, n[i]))

[At, error] = quad(t, a, b)
print(At)
