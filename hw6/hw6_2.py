# author: Natasha Latouf

# Problem 2

import numpy as np
from scipy.integrate import quad
import math


# Define a function detailing the general formula of the trapezoidal rule
def my_trapezoidal(g, n, a, b):
    # Find the width of the rectangle part of the trapezoid
    x, w = np.linspace(a, b, num=n+1, retstep=True)
    # Find the change in x between points
    h = (b - a)/n
    # Find the initial condition
    s = g(x[0])
    # Set up a for loop to loop through the different values of n
    for i in range(1, n):
        s += 2 * g(x[i])
    # Return the final answer for the trapezoidal area
    return (s + g(x[-1])) * h/2

# Define each function given in the problem


def f(x):
    return x**4


def g(x):
    return 2/(x - 4)


def c(x):
    return (x**2)*(math.log(x))


def t(x):
    return np.exp(2*x)*np.sin(2*x)


# Input the bounds, and set n as an array containing 1, 10, 100, 1000.
a = 0
b = 4
n = np.array([1, 10, 100, 1000])
# Make a for loop that will use the trapezoidal definition with the function defined above
for i in range(len(n)):
    print(my_trapezoidal(f, n[i], a, b))
# Calculate the actual value of the integral
[Af, error] = quad(f, a, b)
print(Af)

# Repeat for each different function, and have each value print to the console
a = 0
b = 3

for i in range(len(n)):
    print(my_trapezoidal(g, n[i], a, b))

[Ag, error] = quad(g, a, b)
print(Ag)


a = 1
b = 4

for i in range(len(n)):
    print(my_trapezoidal(c, n[i], a, b))

[Ac, error] = quad(c, a, b)
print(Ac)


a = 0
b = 2*np.pi

for i in range(len(n)):
    print(my_trapezoidal(t, n[i], a, b))

[At1, error] = quad(t, a, b)
print(At1)
