# author: Natasha Latouf

# Homework 2

import numpy as np
import matplotlib.pyplot as plt

# Problem 5

# 5.1

# Input the start point, stop point, and the increment value
stop = 10
start = 0
inc = 0.5
# Calculate the number of points with a simple equation
p = int(1+(stop-start)/inc)
# Specify the start point, start point, and number of points, defined as p
x = np.linspace(0, 10, p)
# Create an empty plot, and a definition for y
n = x.size
y = np.empty(n)
# Input the equation needed
for i in range(x.size):
    y[i] = x[i]**2
# Plot the x and y values, and show the plot
plt.plot(x, y)
print('5.1')
plt.show()
plt.clf()

# 5.2

# Repeat the above steps from 5.1
stop = 4*np.pi
start = 0
inc = np.pi/4
p = int(1+(stop-start)/inc)

x = np.linspace(0, 4*np.pi, p)
n = x.size
y = np.empty(n)

for i in range(x.size):
    y[i] = np.sin(x[i])

plt.plot(x, y)
print('5.2')
plt.show()
plt.clf()

# 5.3

# Repeat the above steps from 5.1
stop = 4*np.pi
start = 0
inc = np.pi
p = int(1+(stop-start)/inc)

x = np.linspace(0, 4*np.pi, p)
n = x.size
y = np.empty(n)

for i in range(x.size):
    y[i] = np.sin(x[i])

plt.plot(x, y)
print('5.3')
plt.show()
plt.clf()

# 5.4

# Repeat the same steps as above in 5.1
theta = np.linspace(-8*np.pi, 8*np.pi)
n = theta.size
r = np.empty(n)

for i in range(theta.size):
    r[i] = np.e**(0.1*theta[i])
# Since the equation is written in theta and r, convert the values into x and y before plotting
x = r*np.cos(theta)
y = r*np.sin(theta)

plt.plot(x, y)
print('5.4')
plt.show()
plt.clf()
