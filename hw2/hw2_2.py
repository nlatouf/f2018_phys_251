# author: Natasha Latouf

# Homework 2

import numpy as np

# Problem 2

# Define x with the range from 1 to 101, to exclude 0 and include 100
x = range(1, 101)
# Shape the array into 10 x 10
x = np.reshape(x, (10, 10))
# Use a for loop to define the length of the rows and columns
for row in range(len(x)):
    for column in range(len(x[row])):
        # Add an if statement to narrow the search for odd # between 40-60 numbers that would have a remainder of 1
        if 40 < x[row, column] < 60 and x[row, column] % 2 == 1:
            print(x[row, column])

