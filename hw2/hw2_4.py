# author: Natasha Latouf

# Homework 2

import numpy as np
import random

# Problem 4

# 4.1

# Make a matrix of range 1 - 100, in a 10 x 10 shape
A = range(1, 101)
A = np.reshape(A, (10, 10))
# Make every number in A a random integer between 7-10
for row in range(len(A)):
    for column in range(len(A[row])):
        A[row, column] = random.randint(7, 10)
print(A)

# 4.2

# Print the 3rd row
print(A[2, :])

# Print the 3rd column
print(A[:, 2])

# 4.3

# Recreate matrix A under name B
B = range(1, 101)
B = np.reshape(B, (10, 10))
for row in range(len(B)):
    for column in range(len(B[row])):
        B[row, column] = random.randint(7, 10)
# Replace every number in the last column with a 99, by making every number 0 and adding 99
B[:, 9] = B[:, 9]*0 + 99

print(B)

# 4.4

# Recreate matrix A under name C
C = range(1, 101)
C = np.reshape(C, (10, 10))
for row in range(len(C)):
    for column in range(len(C[row])):
        C[row, column] = random.randint(7, 10)
# Search the matrix for all numbers that == 8, and replace them with -1
for x in range(len(C)):
    for y in range(len(C[x])):
        if C[x, y] == 8:
            C[x, y] = -1

print(C)
