# author: Natasha Latouf

# Homework 2

# Problem 1

# 1.1

# Define a function for the summation

def custom_sum_1():
    # Clarify the end # n, and s must == 0
    n = 5
    s = 0
    # Use a for loop to increase k from 0 in increments of 1
    for k in range(n + 1):
        # input the equation
        s += 1/(2**k)

    return s

# Call the defined function and print the answer

s_1 = custom_sum_1()

print(s_1)

# 1.2
# Repeat same steps from above
def custom_sum_2():

    n = 5
    s = 0
    # Since the starting point is 2 and not 0, it must be placed in the k range
    for k in range(2, n + 1):
        s += 4/(3**k)

    return s

# Call a different variable for the functions for ease


s_2 = custom_sum_2()

print(s_2)

# 1.3
# Repeat all steps a third time, changing the starting point in k range


def custom_sum_3():

    n = 5
    s = 0
    for k in range(1, n + 1):
        s += ((3/2**k) - (2/5**k))
    return s


s_3 = custom_sum_3()

print(s_3)
