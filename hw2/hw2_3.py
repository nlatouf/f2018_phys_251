# author: Natasha Latouf

# Homework 2

import numpy as np

# Problem 3

# Form a matrix that goes from 1 - 100, and shape it into a 10 x 10 matrix
x = range(1, 101)
x = np.reshape(x, (10, 10))
# Form a second matrix of the same range and shape
y = range(1, 101)
y = np.reshape(y, (10, 10))
# Change the orientation of the rows by setting the columns and rows of matrix y to the reverse of matrix x
for row in range(len(x)):
    for column in range(len(x[row])):
        y[column, row] = x[row, column]
# Print both
print(x)
print(y)

