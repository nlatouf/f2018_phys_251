# author: Natasha Latouf

# Homework 4

# Question 3
# Import the necessary libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
# Import the necessary file from your computer
dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Downloads'
dname2 = r'class_data_01'
fname = 'free_fall_run18_csv.csv'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
freef = np.genfromtxt(dfname, delimiter=',', skip_header=1)
# Separate the time and position from the data, and form an empty array of length k
t = freef[:, 0]
p = freef[:, 1]
k = len(t)
# Create global sums
global n
global sum_1
global sum_2
# Specify the length of n matches the length of t and set the sums equal to 0
n = len(t)
sum_1 = 0
sum_2 = 0
# Create the first sum
for i in range(len(t)):
    sum_1 += t[i]
# Calculate the given equation for average
ave = (1/n)*sum_1
print(ave)
# Calculate the sum needed for the sigma function
for i in range(len(t)):
    sum_2 += (t[i] - ave)**2
# Calculate the given equation for sigma
sigma = np.sqrt(sum_2/n-1)
print(sigma)
# Calculate velocity using the slope function
for i in range(k-1):
        v = (p[i + 1] - p[i]) / (t[i + 1] - t[i])


# Define the standard equation of a parabola
def myline(t, a, b, c):
    y = a*t**2 + b*t + c
    return y


# Plot every parabola's curve fit, based on it's bounds which can be input into curve_fit
popt1, pcov1 = curve_fit(myline, t, p, bounds=(1, 2))
a1 = popt1[0]
b1 = popt1[1]
c1 = popt1[2]
y_hat1 = myline(t, a1, b1, c1)

popt2, pcov2 = curve_fit(myline, t, p, bounds=(2, 2.5))
a2 = popt2[0]
b2 = popt2[1]
c2 = popt2[2]
y_hat2 = myline(t, a2, b2, c2)
t += .1

popt3, pcov3 = curve_fit(myline, t, p, bounds=(2.6, 3.1))
a3 = popt3[0]
b3 = popt3[1]
c3 = popt3[2]
y_hat3 = myline(t, a3, b3, c3)

popt4, pcov4 = curve_fit(myline, t, p, bounds=(3.1, 3.7))
a4 = popt4[0]
b4 = popt4[1]
c4 = popt4[2]
y_hat4 = myline(t, a4, b4, c4)

popt5, pcov5 = curve_fit(myline, t, p, bounds=(3.8, 4))
a5 = popt5[0]
b5 = popt5[1]
c5 = popt5[2]
y_hat5 = myline(t, a5, b5, c5)

popt6, pcov6 = curve_fit(myline, t, p, bounds=(4.1, 4.4))
a6 = popt6[0]
b6 = popt6[1]
c6 = popt6[2]
y_hat6 = myline(t, a6, b6, c6)

popt7, pcov7 = curve_fit(myline, t, p, bounds=(4.5, 4.7))
a7 = popt7[0]
b7 = popt7[1]
c7 = popt7[2]
y_hat7 = myline(t, a7, b7, c7)

popt8, pcov8 = curve_fit(myline, t, p, bounds=(4.8, 5))
a8 = popt8[0]
b8 = popt8[1]
c8 = popt8[2]
y_hat8 = myline(t, a8, b8, c8)

# Plot the parabolas
plt.scatter(t, p, label='Parabolas')
plt.grid(True)
plt.legend()
plt.title('Free Fall Data')
plt.xlabel('Time (s)')
plt.ylabel('Position (m)')
# Plot the curve fit line
plt.plot(t, y_hat1)
plt.plot(t, y_hat2)
plt.plot(t, y_hat3)
plt.plot(t, y_hat4)
plt.plot(t, y_hat5)
plt.plot(t, y_hat6)
plt.plot(t, y_hat7)
plt.plot(t, y_hat8)

plt.show()

# For whatever reason, the curve fit lines are extremely large, and not on the appropriate scale the the parabolas.
# I have no idea why, and I also have no clue how to fix it. This was my best attempt.