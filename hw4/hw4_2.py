# author: Natasha Latouf

# Homework 4

# Question 2
# Import the nrcessary libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
# Call the necessary file from your computer
dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Downloads'
dname2 = r'class_data_01'
fname = 'sinusoidal_data.csv'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
sindat = np.genfromtxt(dfname, delimiter=',')
# Select the time and position from the data
t = sindat[:, 0]
v = sindat[:, 1]

# Define the equation as given in the question
def volt(t, a, b, c):
    v = a*np.exp(-b*t)*np.sin(c*t)
    return v


# Provide the starting constants
# Volts
a = .5
# Seconds
b = 125
# Seconds
c = 5 * 10**3
# Form a list with those constants
guess = [a, b, c]
# Calculate the curve fit of the equation, variables, and the list of constants
popt, pcov = curve_fit(volt, t, v, guess)
# Define the best fit constants
a1 = popt[0]
b1 = popt[1]
c1 = popt[2]
# Rename the equation
yhat = volt(t, a1, b1, c1)
# Plot the original data, with a grid, title, and axis labels
plt.scatter(t, v, marker='o', linewidth=0, label='Sinusoidal')
plt.grid(True)
plt.title('Sinusoidal')
plt.legend()
plt.xlabel('Time (s)')
plt.ylabel('Voltage (V)')
# Plot the best fit curve
plt.plot(t, yhat, label='Opt')
plt.show()