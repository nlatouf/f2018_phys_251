# author: Natasha Latouf

# Homework 4

# Problem 1

# Import all necessary libraries
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
# Import the all necessary data by locating the file, reading into it, and skipping the header
dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Downloads'
dname2 = r'class_data_01'
fname = 'dat_10.txt'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
data01 = np.genfromtxt(dfname, skip_header=1)

dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Downloads'
dname2 = r'class_data_01'
fname = 'dat_11.txt'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
data02 = np.genfromtxt(dfname, skip_header=1)

dname = r'/Users'
dname0 = r'natashalatouf'
dname1 = r'Downloads'
dname2 = r'class_data_01'
fname = 'dat_12.txt'
dfname = '{0:s}/{1:s}/{2:s}/{3:s}/{4:s}' .format(dname, dname0, dname1, dname2, fname)
data03 = np.genfromtxt(dfname, skip_header=1)
# Define time and position for each set of data
t1 = data01[:, 0]
p1 = data01[:, 1]

t2 = data02[:, 0]
p2 = data02[:, 1]

t3 = data03[:, 0]
p3 = data03[:, 1]
# Define the length of a variable as equal as time
k1 = len(t1)
k2 = len(t2)
k3 = len(t3)
# Make an array full of zeroes for the velocities to fill
v1 = np.zeros(k1-1, dtype=float)
v2 = np.zeros(k2-1, dtype=float)
v3 = np.zeros(k3-1, dtype=float)
# Calculate the velocity by finding the slope of position and time, p2-p1/t2-t1
for i in range(k1-1):
    v1[i] = (p1[i+1]-p1[i])/(t1[i+1]-t1[i])

for i in range(k2-1):
    v2[i] = (p1[i+1]-p1[i])/(t1[i+1]-t1[i])

for i in range(k3-1):
    v3[i] = (p1[i+1]-p1[i])/(t1[i+1]-t1[i])
# set x = t, and make an array for it, and set y = position
x1 = t1
x_a = np.array([x1])
y1 = p1
# Apply the global function to be able to use the sums everywhere
global n
global sum_1
global sum_2
global sum_3
global sum_4
global sum_5
# Set n as the length of x1, therefore t1, and set all the sums equal to 0
n = len(x1)
sum_1 = 0
sum_2 = 0
sum_3 = 0
sum_4 = 0
sum_5 = 0
# Set the equation for each sum
for i in range(len(x1)):
    sum_1 += x1[i]*y1[i]
    sum_2 += x1[i]
    sum_3 += y1[i]
    sum_4 += (x1[i])**2
    sum_5 += x1[i]
sum_5 = sum_5**2

y_bar = 1/n*sum_3

# Define the error in m
def m(x1, y1):
    m1 = (n*sum_1 - sum_2*sum_3)/(n*sum_4 - sum_5)
    return m1


print(m(x1, y1))

# Define the error in b
def b(x1, y1):
    b1 = (sum_4*sum_3 - sum_1*sum_2)/(n*sum_4 - sum_5)
    return b1


print(b(x1, y1))

y_hat1 = np.array([])
for i in range(len(x1)):
    y_hat1 = np.append(y_hat1, m(x1, y1)*x_a[0][i]+b(x1, y1))

# Define the error in R2, with extra sums
def R2(y):
    sum_6 = 0
    sum_7 = 0
    for i in range(len(x1)):
        sum_6 += (y_hat1[i] - y1[i])**2
        sum_7 += (y_bar - y1[i])**2
    R2 = 1 - sum_6/sum_7
    return R2


print(R2(y1))

# Define the function to use in curve fitting, the equation of a line
def myline(x, m, b):
    y = m*x+b
    return y

# Fit a line to each of the sets of data, subtracting one from t so the arrays are the same size
popt1, pcov1 = curve_fit(myline, t1[:-1], v1)
popt2, pcov2 = curve_fit(myline, t2[:-1], v2)
popt3, pcov3 = curve_fit(myline, t3[:-1], v3)
# Define the optimal slope
m1 = popt1[0]
m2 = popt2[0]
m3 = popt3[0]
# Define the optimal starting point
b1 = popt1[1]
b2 = popt2[1]
b3 = popt3[1]
# Define the line to be plotted
yhat1 = myline(x1, m1, b1)
yhat2 = myline(x1, m2, b2)
yhat3 = myline(x1, m3, b3)

# Plot the original data as a scatter plot, with labels
plt.scatter(t1[:-1], v1, label='data10')
plt.scatter(t2[:-1], v2, label='data11')
plt.scatter(t3[:-1], v3, label='data12')
# Input a title, grid, legend, and axis labels
plt.title('Time vs Velocity')
plt.grid(True)
plt.legend()
plt.xlabel('Time (s)')
plt.ylabel('Velocity (m/s)')
# Plot the optimal line calculated with curve fit
plt.plot(t1, yhat1, label='Data 10 Curve Fit')
plt.plot(t2, yhat2, label='Data 11 Curve Fit')
plt.plot(t3, yhat3, label='Data 12 Curve Fit')
plt.show()
