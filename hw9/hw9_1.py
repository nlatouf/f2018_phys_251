# author: Natasha Latouf

# Homework 9

# Problem 1
# import the necessary libraries
import numpy as np
import matplotlib.pyplot as plt
# Input the initial conditions
y0 = 1000
t0 = 0
tn = 10
# Set up the delta t's as an array
dt = np.asarray([.01, .1, 1])
# Input the gravity constant
g = 9.8
# Establish the figure
plt.figure(1)
# Set up a for loop that will loop through all of the dt values
for idt in range(len(dt)):
    dti = dt[idt]
    # Establish nk, which will be the amount of steps
    nk = int(((tn-t0)/dti) + 1)
    # Set up empty arrays
    y = np.zeros(nk, dtype=float)
    t = np.zeros(nk, dtype=float)
    # Establish that the first value in the array is the initial condition
    y[0] = y0
    t[0] = t0
    # Set up another for loop that contains the definition for the Euler equation
    for k in range(nk - 1):
        y[k + 1] = y[k] - g*dti*t[k]
        t[k + 1] = t[k] + dti
    # Plot all the  solutions for every delta t
    plt.plot(t, y, '.', label=r'$\Delta = {}$'.format(dti))
# Add labels, a grid, and a legend
plt.xlabel('Time(s)')
plt.ylabel('y')
plt.grid(True)
plt.legend()
plt.show()
