# author: Natasha Latouf

# Homework 9

# Problem 2
# Import the necessary libraries
import numpy as np
import matplotlib.pyplot as plt
# Input the initial conditions
g = 9.8
t0 = 0
tn = 10
v0 = 0
y0 = 0
# Set up the delta t's as an array
dt = np.asarray([0.01, 0.1, 1])

plt.figure(1)
for idt in range(len(dt)):
    dti = dt[idt]
    # Establish nk, which will be the amount of steps
    nk = int(((tn-t0)/dti) + 1)
    # Set up empty arrays
    y = np.zeros(nk, dtype=float)
    t = np.zeros(nk, dtype=float)

    yE = np.zeros(nk, dtype=float)
    vE = np.zeros(nk)

    yEC = np.zeros(nk, dtype=float)
    vEC = np.zeros(nk)
    # Establish that the first value in the array is the initial condition
    t[0] = t0
    yE[0] = y0
    vE[0] = v0
    yEC[0] = y0
    vEC[0] = v0
    # Set up another for loop that contains the definition for the Euler and Euler-Cromer equation
    for k in range(nk - 1):
        # Euler
        vE[k + 1] = vE[k] - dti * g
        yE[k + 1] = yE[k] + dti * vE[k]
        t[k + 1] = t[k] + dti
        # Euler Cromer
        vEC[k + 1] = vEC[k] - dti * g
        yEC[k + 1] = yEC[k] + dti * vEC[k + 1]

    # Plot the positions from each equation
    plt.plot(t, yE, '.', label='Position' + 'r$\Delta = {}$'.format(dti))
    plt.plot(t, yEC, '.', label='Position EC' + 'r$\Delta = {}$'.format(dti))
# Add labels, legends, and a grid
plt.legend()
plt.grid(True)
plt.xlabel('Time(s)')
plt.ylabel('Position(m)')
plt.show()

# Repeat the process to plot velocities
plt.figure(2)
for idt in range(len(dt)):
    dti = dt[idt]
    # Establish nk, which will be the amount of steps
    nk = int(((tn-t0)/dti) + 1)
    # Set up empty arrays
    y = np.zeros(nk, dtype=float)
    t = np.zeros(nk, dtype=float)
    # Establish that the first value in the array is the initial condition

    yE = np.zeros(nk, dtype=float)
    vE = np.zeros(nk)

    yEC = np.zeros(nk, dtype=float)
    vEC = np.zeros(nk)

    t[0] = t0
    yE[0] = y0
    vE[0] = v0

    yEC[0] = y0
    vEC[0] = v0

    for k in range(nk - 1):
        # Euler
        vE[k + 1] = vE[k] - dti * g
        yE[k + 1] = yE[k] + dti * vE[k]
        t[k + 1] = t[k] + dti

        # Euler Cromer
        vEC[k + 1] = vEC[k] - dti * g
        yEC[k + 1] = yEC[k] + dti * vEC[k + 1]
        t[k + 1] = t[k] + dti
    plt.plot(t, vE, '.', label='Velocity' + 'r$\Delta = {}$'.format(dti))
    plt.plot(t, vEC, '.', label='Velocity EC' + 'r$\Delta = {}$'.format(dti))

plt.legend()
plt.grid(True)
plt.xlabel('Time(s)')
plt.ylabel('Velocity(m/s)')
plt.show()
