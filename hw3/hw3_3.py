# author: Natasha Latouf

# Homework 3

import numpy as np
import matplotlib.pyplot as plt

# Problem 3

# Input the given values, changing degrees to radians, as this is what python accepts
a = (45*np.pi)/180
v_0 = 40.0
g = 9.8
# Make the initial positions 0 for simplicity
x_0 = 0
y_0 = 0
# Input the equations to find the components of velocity
v_0x = v_0*np.cos(a)
v_0y = v_0*np.sin(a)
# Input the time 0 - 7, with 100 points, and set up empty arrays for every variable that will been to be solved for
t = np.linspace(0, 7, 100)
n = t.size
y = np.empty(n)
x = np.empty(n)
v_y = np.empty(n)
v_x = np.empty(n)
# Input every equation needed, and calculate position and time in components
for i in range(t.size):
    x[i] = x_0 + v_0x * t[i]

for i in range(t.size):
    y[i] = y_0 + v_0y * t[i] - (1 / 2) * g * t[i] ** 2

for i in range(t.size):
    v_x[i] = v_0x

for i in range(t.size):
    v_y[i] = v_0y - g * t[i]
# Plot position versus time for x and y, labelled with position and time in meters and seconds
plt.plot(t, x)
plt.text(1, 55, 'x vs time', alpha=1.0, color='#922B21')
plt.plot(t, y)
plt.text(5.7, 5, 'y vs time', alpha=1.0, color='#9B59B6')
plt.title('Position vs Time')
plt.xlabel('Time (s)')
plt.ylabel('Position (m)')
plt.grid(True)
print('3.1')
plt.savefig('3.1.png')
plt.show()
plt.clf()
# Plot velocity versus time for x and y, labelled with position and time in meters and seconds
plt.plot(t, v_x)
plt.text(3, 25, 't vs v\u2093', alpha=1.0, color='#C0392B')
plt.plot(t, v_y)
plt.text(3, 1, 'y vs v_y', alpha=1.0, color='#512E5F')
plt.title('Velocity vs Time')
plt.xlabel('Time (s)')
plt.ylabel('Velocity (m/s)')
plt.grid(True)
print('3.2')
plt.savefig('3.2.png')
plt.show()
plt.clf()
# Plot position of x versus position of y, labelled with position in meters
plt.plot(x, y)
plt.text(15, 10, 'Trajectory', alpha=1.0, color='#154360')
plt.title('Trajectory')
plt.xlabel('Position x (m)')
plt.ylabel('Position y (m)')
plt.grid(True)
print('3.3')
plt.savefig('3.3.png')
plt.show()
plt.clf()