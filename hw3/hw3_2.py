# author: Natasha Latouf

# Homework 3

import numpy as np
import matplotlib.pyplot as plt

# Problem 2

# Repeat every step in Problem 1, however, add more points to the plot, and graph a second equation
x = np.linspace(-4, 4, 100)
n = x.size
y = np.empty(n)
y2 = np.empty(n)
# Prepare two different arrays to fill, and input two different equations with for loops
for i in range(x.size):
    y[i] = np.sin(x[i])

for i in range(x.size):
    y2[i] = x[i]**2
# Plot each equation separately, on the same graph
plt.plot(x, y)
plt.text(-2, -.5, 'sin(x)', alpha=1.0, color='#2E8B57')
plt.plot(x, y2)
plt.text(-2, 5, 'x\u00b2', alpha=1.0, color='#BC8F8F')
plt.title('sin(x) vs x\u00b2')
plt.xlabel('x')
plt.ylabel('f(x)')
plt.grid(True)
# Calculate where the two lines intersect
x_int, y_int = (x[np.argmin(np.abs(y-y2))], y[np.argmin(np.abs(y-y2))])
# Print the coordinates of the intersection
print(x_int, y_int)

print('2.1')
plt.savefig('2.1.png')
plt.show()
plt.clf()
# 2.1

# Repeat all the steps in 2.1, including the intersection equation
x = np.linspace(-4, 4, 100)
n = x.size
y = np.empty(n)
y2 = np.empty(n)

for i in range(x.size):
    y[i] = np.sin(2*x[i])

for i in range(x.size):
    y2[i] = ((x[i]**3)/10) + ((x[i]**2)/10)

plt.plot(x, y)
plt.text(-2.5, 1.3, 'sin(2x)', alpha=1.0, color='#696969')
plt.plot(x, y2)
plt.text(2, 4, 'x\u00b3/10 + x\u00b2/10', alpha=1.0, color='#000080')
plt.title('sin(2x) vs x\u00b3/10 + x\u00b2/10')
plt.xlabel('x')
plt.ylabel('f(x)')
plt.grid(True)

x_int, y_int = (x[np.argmin(np.abs(y-y2))], y[np.argmin(np.abs(y-y2))])
print(x_int, y_int)

print('2.2')
plt.savefig('2.2.png')
plt.show()
plt.clf()
# 2.2

# Once again, redefine the equations, find the intersection, and plot it
x = np.linspace(-6, 6, 100)
n = x.size
y = np.empty(n)
y2 = np.empty(n)

for i in range(x.size):
    y[i] = (np.exp(0.001*x[i])) + np.log(x[i]**3)

for i in range(x.size):
    y2[i] = (.1*x[i]**3) + (.1*x[i]**2) - 5

plt.plot(x, y)
plt.text(1, 5, 'e^.001x + ln(x\u00b3)', alpha=1.0, color='#8B0000')
plt.plot(x, y2)
plt.text(-3, -8, '.1x\u00b3 +.1x\u00b2 -5', alpha=1.0, color='#000000')
plt.title('e^.001x + ln(x\u00b3) vs .1x\u00b3 +.1x\u00b2 -5')
plt.xlabel('x')
plt.ylabel('f(x)')
plt.grid(True)

x_int, y_int = (x[np.argmin(np.abs(y-y2))], y[np.argmin(np.abs(y-y2))])
print(x_int, y_int)

print('2.3')
plt.savefig('2.3.png')
plt.show()
plt.clf()
# 2.3
