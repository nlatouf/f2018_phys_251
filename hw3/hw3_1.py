# author: Natasha Latouf

# Homework 3

import numpy as np
import matplotlib.pyplot as plt

# Problem 1
# First, specify the start point, the end point, and the number of points to be plotted
theta = np.linspace(-np.pi, np.pi, 50)
# Create an empty array to fill with y
n = theta.size
r = np.empty(n)
# Input the given equation
for i in range(theta.size):
    r[i] = np.cos(2*theta[i])
# Plot the coordinates that have been found
plt.plot(theta, r)
# Add in line labels by specifying where the text will be, what to print, opacity, and color
plt.text(-.8, -.25, 'cos2'+r'$\Theta$', alpha=1.0, color='#4682B4')
# Add a title to the graph, using a specific operation for special characters
plt.title('Cosine'+r'$\Theta$')
# Add an x label
plt.xlabel('x')
# Add a y label
plt.ylabel('f(x)')
# Add a grid to the graph
plt.grid(True)
# Print the figure to alert you that the graph is ready
print('1.1')
# Save the figure as a png file
plt.savefig('1.1.png')
# Show the graph
plt.show()
# Clear the plot to prepare it for the rest of the code
plt.clf()
# 1.1

# Redefine the equation and size, and repeat every step above
theta = np.linspace(-np.pi, np.pi, 50)
n = theta.size
r = np.empty(n)

for i in range(theta.size):
    r[i] = (2*(np.cos(theta[i]))**2)-1

plt.plot(theta, r)
# Add a square or cube to the function with special unicode text
plt.title('2Cos'+'\u00b2'+r'$\Theta$' + '-1')
plt.text(-.8, -.25, '2cos'+'\u00b2'+r'$\Theta$' + '-1', alpha=1.0, color='#5F9EA0')
plt.xlabel('x')
plt.ylabel('f(x)')
plt.grid(True)
print('1.2')
plt.savefig('1.2.png')
plt.show()
plt.clf()
# 1.2

# Repeat every step above, redefining the equation needed
theta = np.linspace(-np.pi, np.pi, 50)
n = theta.size
r = np.empty(n)
# Since secant is not in numpy, you need to use another trig identity
for i in range(theta.size):
    r[i] = (1/np.cos(theta[i]))+np.tan(theta[i])


plt.plot(theta, r)
plt.title('Sec'+'$\Theta$' + 'Tan'+r'$\Theta$')
plt.text(-.5, -4, 'sec'+'$\Theta$' + 'tan'+r'$\Theta$', alpha=1.0, color='#4682B4')
plt.xlabel('x')
plt.ylabel('f(x)')
plt.grid(True)
print('1.3')
plt.savefig('1.3.png')
plt.show()
plt.clf()
# 1.3
